import React from "react";
import ReactDOM from "react-dom";
import "./assets/css/modern-normalize.css";
import "./assets/css/misc.css";
import "./assets/css/style.css";
import "./assets/css/media.css";
import App from "./App";
import "./bootstrap";
import { BrowserRouter } from "react-router-dom";
import {store, persistor} from './redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';


ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<PersistGate persistor={persistor}>
				<App/>
			</PersistGate>
		</BrowserRouter>
	</Provider>
	,
	document.getElementById("root")
);
