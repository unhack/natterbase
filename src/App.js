import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import IndividualInvestment from './views/rienInvestment/individual';
import CDAInvestment from './views/rienInvestment/cda';
import REAInvestment from './views/rienInvestment/rea';
import CorporateInvestment from './views/rienInvestment/corporate';
import GovernmentInvestment from './views/rienInvestment/government';
import Login from './views/Login';
import Dashboard from './views/Dashboard';

const App = () => {
	return (
		<>
			<Switch>
				<Route path='/individual'>
					<IndividualInvestment />
				</Route>
				<Route path='/community-development-association'>
					<CDAInvestment />
				</Route>
				<Route path='/residential-estate-associations'>
					<REAInvestment />
				</Route>
				<Route path='/corporate-organization'>
					<CorporateInvestment />
				</Route>
				<Route path='/government'>
					<GovernmentInvestment />
				</Route>
				<Route path='/login'>
					<Login />
				</Route>
				<Route path='/dashboard'>
					<Dashboard />
				</Route>
				<Redirect to='/' />
			</Switch>
		</>
	);
};

export default App;
