import React from 'react';
import './index.css'

import Header from './header'
import Sidebar from './sidebar';

const Layout = (props) => {
    return (    
        <>
            <div className='dashboard__layout'>
                <Sidebar/>
                <Header/>
                <div className='dashboard__layout__content'>
                    {props.children} 
                </div>
            </div>
        </>
    )
}

export default Layout;