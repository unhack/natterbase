import React from 'react';
import './index.css';

import { NavLink } from 'react-router-dom';

import Logo from '../assets/img/logo.png';
import OverviewIcon from '../assets/icons/overviewIcon.svg';
import Investmenticon from '../assets/icons/investmentIcon.svg';
import ApplicationsIcon from '../assets/icons/applicationsIcon.svg';
import InvestorIcon from '../assets/icons/investorIcon.svg';
import SettingsIcon from '../assets/icons/settingsIcon.svg';

const Sidebar = () => {
	return (
		<>
			<div className='dashboard__sidebar'>
				<div className='dashboard__sidebar__component'>
					<div className='dashboard__logo'>
						<img src={Logo} alt='logo' />
					</div>
					<div className='dashboard__sidebar__nav'>
						<div className='nav__item'>
							<NavLink to='#!'>
								<img src={OverviewIcon} alt='overview' />
							</NavLink>
						</div>
						<div className='nav__item'>
							<NavLink to='#!'>
								<img src={Investmenticon} alt='investment' />
							</NavLink>
						</div>
						<div className='nav__item'>
							<NavLink to='#!'>
								<img src={ApplicationsIcon} alt='applications' />
							</NavLink>
						</div>
						<div className='nav__item'>
							<NavLink to='#!'>
								<img src={InvestorIcon} alt='investor' />
							</NavLink>
						</div>
						<div className='nav__item'>
							<NavLink to='#!'>
								<img src={SettingsIcon} alt='settings' />
							</NavLink>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Sidebar;
