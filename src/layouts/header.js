import React from 'react';
import './index.css'

import { Link } from 'react-router-dom';


import SearchBar from '../components/general-components/searchBar';
import BellIcon from "../assets/icons/bellIcon.svg";
import DropIcon from "../assets/icons/chevronDownIcon.svg";
import Avatar from "../assets/img/avatar.png";
import PowerIcon from "../assets/icons/power-nav.svg";
import headerSettings from "../assets/icons/headerSettings.svg"




const Header = () => {
    return (    
        <>
            <div className='dashboard__layout'>
                <div className='dashboard__header'>
                    <div className='dashboard__header__component'>
                        <div className='dashboard__header__nav custom-90'>
                            <div className='search-col'>
                                <SearchBar placeholder='grey'/>
                            </div>
                             <div className='header__right'>
                                <div className='nav__item'>
                                    <Link to="#!">
                                        <img src={headerSettings} alt='icon'/>
                                    </Link>
                                </div>
                                <div className='nav__item'>
                                    <Link to="#!">
                                        <img src={BellIcon} alt='icon'/>
                                    </Link>
                                </div>
                                <div className='nav__item'>
                                    <Link to="#!">
                                        <img src={Avatar} alt='icon'/>
                                        <span>John</span>
                                        <img src={DropIcon} alt='icon'/>
                                    </Link>
                                </div>
                                <div className='nav__item'>
                                    <Link to="#!">
                                        <img src={PowerIcon} alt='icon' id='logout'/>
                                    </Link>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Header;