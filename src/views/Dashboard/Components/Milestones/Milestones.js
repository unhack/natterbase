import React, { useState } from 'react';

import './Milestones.css'

import vectorMilestone from '../../../../assets/img/empty-milestone.svg'
import Button from '../../../../components/general-components/Button/Button';
import Modal from '../../../../components/general-components/Modal/Modal';
import TextField from '../../../../components/general-components/Input/Input';

const Milestones = () => {
    const [milestones] = useState('')
    const [ecp] = useState('')
    const [modal] = useState(false)

    return (
        <div className='milestones'>
            {
                modal && <Modal/>
            }
            {
                milestones && 
                <div className='empty__milestone'>
                    <div>
                        <img src={vectorMilestone} alt='vector'/>
                        <p>You dont have any milestone at the moment</p>
                        <Button
                            text='Create Milestone'
                            color='green'
                        />
                    </div>
                </div> 
            }
            {
                ecp &&
                <div className='empty__milestone'>
                    <div>
                        <p>Click the button below to add new ECP to this project</p>
                        <Button
                            text='Add New EPC'
                            color='green'
                        />
                    </div>
                </div> 
            }
            {
                <div className='milestone__content'>
                    <div className='main__details'>
                        <p>EPC Admin</p>
                        <p>Jerry Coperation</p>
                        <p>Jerry@neip.com</p>
                        <p>Inactive</p>
                    </div>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th>Milestone Name</th>
                                    <th>% Completion</th>
                                    <th>Expected Deliveries</th>
                                    <th>Estimated Timeline</th>
                                    <th>Status</th>
                                    <th>Others</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Name of milestone</td>
                                    <td>10%</td>
                                    <td>Lorem ipsum dohert </td>
                                    <td>10 weeks</td>
                                    <td>Active</td>
                                    <td>View details</td>
                                </tr>
                                <tr>
                                    <td>
                                        <TextField 
                                            name='milestone'
                                            value=''
                                            placeholder='milestone name'
                                            className='no-border grey-bg'
                                        />
                                    </td>
                                    <td>
                                        <TextField 
                                            name='milestone'
                                            value=''
                                            placeholder='Completion'
                                            className='no-border grey-bg'
                                        />
                                    </td>
                                    <td>
                                        <TextField 
                                            name='milestone'
                                            value=''
                                            placeholder='Deliveries'
                                            className='no-border grey-bg'
                                        />
                                    </td>
                                    <td>
                                        <TextField 
                                            name='milestone'
                                            value=''
                                            placeholder='Timeline'
                                            className='no-border grey-bg'
                                        />
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div className='add__milestone__btn'>
                            <Button
                                text='Add Milestone'
                                color='green'
                            />
                        </div>
                    </div> 
                </div>
            }
        </div>
    )
}

export default Milestones