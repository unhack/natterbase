import React from 'react';

import'./BasicDetails.css'

const BasicDetails = () => {
    return (
        <div className='basic__details'>
            <div className='basic__details__content'>
                <p>full name</p>
                <p>Jerry Chukwu Ibeabuchi</p>
            </div>
            <div className='basic__details__content'>
                <p>House Address</p>
                <p>Lekki Phase II</p>
            </div>
            <div className='basic__details__content'>
                <p>Phone Number</p>
                <p>+23490428484</p>
            </div>
            <div className='basic__details__content'>
                <p>Email Address</p>
                <p>jbl@gmail.com</p>
            </div>
            <div className='basic__details__content'>
                <p>Distribution Company</p>
                <p>DISCO COMPANY</p>
            </div>
        </div>
    )
}

export default BasicDetails;