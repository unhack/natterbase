import React, { useState } from 'react';

import './Repayment.css';
import Button from '../../../../components/general-components/Button/Button';

import vectorMilestone from '../../../../assets/img/empty-milestone.svg'

const Repayment = () => {
    const [repayment] = useState('')

    return (
        <div className='repayment__plan'>
            {
                repayment && 
                <div className='empty__repayment__plan'>
                    <div>
                        <img src={vectorMilestone} alt='vector'/>
                        <p>You dont have any repayment Plan created</p>
                        <Button
                            text='Create Repayment Plan'
                            color='green'
                        />
                    </div>
                </div>
            }
            <div className=''>
                <table>
                    <thead>
                        <tr>
                            <th>Repayment Date</th>
                            <th>Repayment Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>10 June, 2020</td>
                            <td>50,000,000</td>
                        </tr>
                    </tbody>
                </table>
                <div className='add__repayment__plan__btn'>
                    <Button
                        text='Create Repayment Plan'
                        color='green'
                    />
                </div>
            </div>
        </div>
    )
}

export default Repayment;