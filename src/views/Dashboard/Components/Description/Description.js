import React from 'react';

import './Description.css';


const Description = () => {
    return (
        <div className='description__content'>
            <div className='description__content__left'>
                <h2>Description</h2>
                <p>What’s the type of issue, description and category of issue attached to this workflow</p>
            </div>
            <div className='description__content__right'>
                <p>The new processing plant is scheduled to be in operation 24/7 365 days a year and has an estimated energy requirement of up to 3.83 GWh per year. Up to 12% of the total annual energy requirements of the system, i.e. approximately 448.6 MWh, are to be covered in the future by the roof photovoltaic system to be installed. MWh, are to be covered in the future by the roof photovoltaic system to be installed.MWh <a href='/'>Read more </a></p>
            </div>
        </div>
    )
}

export default Description;