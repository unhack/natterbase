import React from 'react';

import './Documents.css';
import Placeholder from '../../../../assets/img/placeholder.svg'

const Documents = () => {
    return (
        <div className='documents__content'>
            <div className='documents__content__left'>
                <h2>Documents</h2>
                <p>What’s the type of issue, description and category of issue attached to this workflow</p>
            </div>
            <div className='documents__content__right'>
                <img src={Placeholder} alt='placeholder'/>
                <img src={Placeholder} alt='placeholder'/>
                <img src={Placeholder} alt='placeholder'/>
            </div>
        </div>
    )   
}

export default Documents