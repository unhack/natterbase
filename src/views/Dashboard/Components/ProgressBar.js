import React from 'react';

import { CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

const ProgressBar = () => {
	return (
		<>
			<div style={{ width: 100, height: 100 }}>
				<CircularProgressbarWithChildren
					value={10}
					strokeWidth={20}
					styles={{
						path: {
							stroke: `rgba(218, 54, 36, ${100})`,
						},
					}}
				>
					<div style={{ color: '#000', opacity: '0.5', fontWeight: 'bold' }}>
						10%
					</div>
				</CircularProgressbarWithChildren>
			</div>
		</>
	);
};

export default ProgressBar;
