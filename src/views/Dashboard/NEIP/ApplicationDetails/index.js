import React from 'react';
import { useHistory } from 'react-router-dom';
import '../../index.css';
import Ellipse from '../../../../assets/img/Ellipse.svg';
import Check from '../../../../assets/img/button-check.svg';
import Summary from '../../Common/Summary';
import Details from '../../Common/Details';

const ApplicationDetails = () => {
	let history = useHistory();
	return (
		<>
			<div className='applications__details pb-2'>
				<div className='custom-80'>
					<div className='applications__details__header'>
						<div>
							<button onClick={() => history.goBack()} className='back__arrow'>
								<i className='fas fa-angle-left'></i> Back
							</button>
						</div>
						<div className='page__title'>
							<div className='page__title__details'>
								<h1>Investment name</h1>
								<p>
									<span className='date'>
										Initiated on Feb 2, 2019 | 12:30am
									</span>
									<span className='divider'>|</span>{' '}
									<span className='owner'>
										{' '}
										by <img src={Ellipse} alt='ellipse' />
										Andrew Milaw
									</span>
								</p>
							</div>
							<div className='page__title__actions'>
								<button>
									<img src={Check} alt='check' /> Push to Disco{' '}
								</button>
							</div>
						</div>
					</div>
					<div>
						<Summary />
					</div>
					<div>
						<Details />
					</div>
				</div>
			</div>
		</>
	);
};

export default ApplicationDetails;
