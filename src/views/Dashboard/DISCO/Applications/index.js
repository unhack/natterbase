import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import '../../index.css';

import Pagination from '../../../../components/general-components/Pagination/Pagination';

const Applications = () => {
	const { path } = useRouteMatch();
	return (
		<>
			<div className='applications__dashboard pb-4'>
				<div className='container-90'>
					<div className='applications__dashboard__header'>
						<h1>Applications</h1>
					</div>
					<div className='applications__dashboard__card'>
						<div className='dashboard__table'>
							<table>
								<thead>
									<tr>
										<th>Full Name</th>
										<th>Phone Number</th>
										<th>Email Address</th>
										<th>Distribution Company</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{new Array(5).fill(1).map((x, i) => (
										<tr key={i}>
											<td>John Michael</td>
											<td>+23492848294</td>
											<td>eko@gmail.com</td>
											<td>Eko Limited</td>
											<td>
												<Link to={`${path}/${i}`}>
													<button>View More</button>
												</Link>
											</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
						<Pagination />
					</div>
				</div>
			</div>
		</>
	);
};

export default Applications;
