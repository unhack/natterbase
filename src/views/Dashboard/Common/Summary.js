import React from 'react';
import './index.css';
import ProgressBar from '../Components/ProgressBar';
import User from '../../../assets/img/Ellipse.svg'

const Summary = () => {
    
    return (
        <>
            <div className='summary__card'>
                <div className='summary__card__content'>
                    <div className='progress-section'>
                        <div>
                            <div className='progress_container'>
                                <ProgressBar/>
                            </div>
                            <p className='sold'>10% sold</p>
                            <p className='total'>Out of 100 in total</p>
                        </div>
                    </div>
                    <div className='investment__details'>
                        <div className='investment__details__content'>
                            <p className='title'>Investment Name</p>
                            <h1 className='investment-title'>Ikeja Prepaid Electricity Meter</h1>
                            <div className='line-break'></div>
                            <div className='owner'>
                                <span className='key'>Created by</span>
                                <span className='value'><img src={User} alt='user'/>Andrew Milov</span>
                                <span className='badge'>Partner</span>
                            </div>
                            <div className='line-break'></div>
                            <div className='finance'>
                                <div>
                                    <span className='key'>Project Type</span> <span className='value'>...</span> 
                                </div>
                                <div>
                                    <span className='key'>Funds needed</span> <span className='value'>N50,000,000</span>
                                </div>
                            </div>
                            <div className='line-break'></div>
                            <div className='finance'>
                                <div>
                                    <span className='key'>Duration</span> <span className='value'>5 years term</span> 
                                </div>
                                <div>
                                    <span className='key'>Amount Raised</span> <span className='value'>N10,000,000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Summary