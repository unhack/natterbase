import React, { useState } from 'react';

import BasicDetails from '../Components/BasicDetails/BasicDetails';
import Milestones from '../Components/Milestones/Milestones';
import Repayment from '../Components/Repayment/Repayment';

import Documents from '../Components/Documents/Documents';
import Description from '../Components/Description/Description';


const Details = () => {
    const [view, setView] = useState('description');

    const switchView = (view) => {
        switch(view){
            case 'description':
                return <Description/>
            case 'documents':
                return <Documents/>
            case 'milestones':
                return <Milestones/>
            case 'details':
                return <BasicDetails/>
            case 'repayment':
                return <Repayment/>
            default:
                return 
        }
    }

    const handleView = (view) => {
        setView(view)
    }

    return (
        <div className='details__card'>
            <div className='details__card__tabs'>
                <p 
                    className={`${view === 'description' && 'active'}`} 
                    onClick={() => handleView('description')}>
                    Description
                </p>
                <p 
                    className={`${view === 'documents' && 'active'}`} 
                    onClick={() => handleView('documents')}>
                    Documents
                </p>
                <p 
                    className={`${view === 'details' && 'active'}`} 
                    onClick={() => handleView('details')}>
                    Other Details
                </p>
                <p 
                    className={`${view === 'milestones' && 'active'}`} 
                    onClick={() => handleView('milestones')}>
                    Milestones
                </p>
                <p 
                    className={`${view === 'repayment' && 'active'}`} 
                    onClick={() => handleView('repayment')}>
                    Repayment
                </p>
            </div>
            <div className='details__card__content'>
                {switchView(view)}
            </div>
        </div>
    )
}

export default Details;