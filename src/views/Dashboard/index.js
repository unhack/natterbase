import React from 'react';
import { Route, useRouteMatch } from 'react-router-dom';
import NEIP from './NEIP';
import DISCO from './DISCO';
import NERC from './NERC';

const Dashboard = () => {
	const { path } = useRouteMatch();

	return (
		<>
			<Route path={`${path}/neip`}>
				<NEIP />
			</Route>
			<Route path={`${path}/disco`}>
				<DISCO />
			</Route>
			<Route path={`${path}/nerc`}>
				<NERC />
			</Route>
		</>
	);
};

export default Dashboard;
