import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Layout from '../../../layouts';
import Applications from './Applications';
import ApplicationDetails from './ApplicationDetails'

const NEIP = () => {
    const { path } = useRouteMatch()
    return (
        <>
            <Switch>
                <Route exact path={`${path}/applications`}>
                    <Layout>
                        <Applications/>
                    </Layout>
                </Route>
                <Route path={`${path}/applications/:id`}>
                    <Layout>
                        <ApplicationDetails/>
                    </Layout>
                </Route>
            </Switch>
        </>
    )
}

export default NEIP