import React from "react";
import PageHeader from "../../../components/general-components/page-header";
import CDAForm from "./form";




export default function REAInvestment() {

    const individualHeading = {
        title: "Residential Estate Associations",
        subtitle: ""
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={individualHeading} />
            <CDAForm />
        </div>
    );
}
