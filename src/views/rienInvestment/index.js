import React from "react";
import {
    Route, Switch, Redirect, useRouteMatch
} from "react-router-dom";
import RienLandingHome from "./home";
import IndividualInvestment from "./individual";
import CDAInvestment from "./cda";
import REAInvestment from "./rea";
import CorporateInvestment from "./corporate";
import GovernmentInvestment from "./government";



export default function RienHome() {
    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route exact path={`${path}`}>
                <RienLandingHome />
            </Route>
            <Route path={`${path}/individual`}>
                <IndividualInvestment />
            </Route>
            <Route path={`${path}/community-development-association`}>
                <CDAInvestment />
            </Route>
            <Route path={`${path}/residential-estate-associations`}>
                <REAInvestment />
            </Route>
            <Route path={`${path}/corporate-organization`}>
                <CorporateInvestment />
            </Route>
            <Route path={`${path}/government`}>
                <GovernmentInvestment />
            </Route>
            <Redirect to="/" /> 
        </Switch>
    );
}
