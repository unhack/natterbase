import React, { useState } from "react";
import Radio from "../../../components/general-components/form/radio";
import Loader from "../../../components/general-components/Loader/Loader";
import { postCall } from "../../../network";
import { urls } from "../../../network/url";
import SuccessMessage from "../../../components/general-components/SuccessMessage/SuccessMessage";
import { checkEmpty } from "../../../util/utils";
import useFileHook from "../../../components/general-components/uploadHook";
import FileUpload from "../../../components/general-components/fileUpload";
import toaster from "toasted-notes";
import { connect } from "react-redux"

import "toasted-notes/src/styles.css"; // optional styles

const IndividualForm = (props) => {
	const [field, setFields] = useState({
		feasibilityStudy: "No",
		projectDesign: "No",
		financialProjection: "No",
	});
	const [url, setUrl] = useState({
		link: "",
	});

	const { handleImageChange, docs } = useFileHook;
	console.log(useFileHook)
	

	const [state, setState] = useState({
		name: "",
		phone: "",
		address: "",
		email: "",
		distributionCompany: "",
		utilityAccountNumber: "",
		meterNo: "",
		businessDistrict: "",
		projectType: "",
		estimatedValue: "",
		projectDescription: "",
	});

	const [loading, setLoading] = useState(false);
	const [successful, setSuccessful] = useState(false);

	const handleRadio = (event) => {
		setFields({
			...field,
			[event.target.name]: event.target.value, //define new key-value pair with new uuid and [].
		});
	};

	const handleFields = (event) => {
		setState({
			...state,
			[event.target.name]: event.target.value,
		});
		
	};

	const submitForm = async (e) => {
		setLoading(true);
		const data = {
			name: state.name,
			email: state.email,
			mobile: state.phone,
			distribution_company_id: state.distributionCompany,
			project_type_id: state.projectType,
			project_description: state.projectDescription,
			business_district: state.businessDistrict,
			category: "individual",
			investment_amount: state.estimatedValue,
			utility_account_number: state.utilityAccountNumber,
			meter_number: state.meterNo,
			feasibility_study: docs.feasibilityStudyUrl,
			project_design: docs.projectDesignUrl,
			financial_projection: docs.financialProjectionUrl,
			kyc: docs.idCardUrl,
			kyc_2: docs.utilityBillUrl,
		};

		const finalData = JSON.stringify(data);
		const response = await postCall(urls.rienRegistration, finalData);
		if (typeof response !== "undefined" && response.data.success === true) {
			setLoading(false);
			setSuccessful(true);
			setUrl({
				...url,
				link: response.data.data.data.authorization_url, //define new key-value pair with new uuid and [].
			});
		} else {
			setLoading(false);
			toaster.notify("Please provide all required fields", {
				duration: null,
				position: "bottom",
			});
		}
	};

	return (
		<div className="neip-standard--section form-section neip-section--100 overflow-visible">
			{loading ? (
				<Loader>
					<h2>Registration in Progress</h2>
					<p>
						Hold on to your seatbelt, its about <br /> to be a journey{" "}
					</p>
				</Loader>
			) : null}
			{successful ? (
				<SuccessMessage>
					<h2>Just one more step left to complete your registration</h2>
					<p>
						You are required to provide an{" "}
						<span className="text-bold color-black">amount</span> to kickstart
						your investemnt. We'll be in touch once your payment has been
						received.{" "}
					</p>

					<a href={url.link} className="neip-cta neip-cta--lg neip-cta--center">
						Proceed to Payment
					</a>
				</SuccessMessage>
			) : (
				<div className="container">
					<div className="neip-section fadeTextUp">
						<div className="neip-section--100 form-wrap">
							<div className="form-wrap">
								<div className="input-field--wrap">
									<input
										className="input-field"
										name="name"
										type="text"
										value={field.name}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Full Name
									</label>
								</div>
								<div className="input-field--wrap">
									<input
										className="input-field"
										name="address"
										type="text"
										value={field.address}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										House Address
									</label>
								</div>
								<div className="input-field--wrap">
									<input
										className="input-field"
										name="phone"
										type="tel"
										value={field.phone}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Phone Number
									</label>
								</div>
								<div className="input-field--wrap">
									<input
										className="input-field"
										name="email"
										type="email"
										value={field.email}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Email Address
									</label>
								</div>

								<div className="input-field--wrap">
									<select
										className="input-field"
										name="distributionCompany"
										onChange={handleFields}
									>
										<option value="" selected disabled>
											Select
										</option>
										<option value="1">Eko</option>
										<option value="2">Ikeja</option>
										<option value="3">Benin</option>
										<option value="4">Enugu</option>
										<option value="5">Port Harcourt</option>
										<option value="6">Kano</option>
										<option value="7">Kaduna</option>
										<option value="8">Yola</option>
										<option value="9">Abuja</option>
										<option value="10">Ibadan</option>
										<option value="11">Jos</option>
									</select>
									<label className="label label-alt label-active">
										Distribution Company
									</label>
								</div>

								<div className="input-field--wrap">
									<input
										className="input-field"
										name="utilityAccountNumber"
										type="tel"
										value={field.utilityAccountNumber}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Utility account number
									</label>
								</div>

								<div className="input-field--wrap">
									<input
										className="input-field"
										name="meterNo"
										type="tel"
										value={field.meterNo}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Meter Number
									</label>
								</div>

								<div className="input-field--wrap">
									<input
										className="input-field"
										name="businessDistrict"
										type="text"
										value={field.businessDistrict}
										onChange={handleFields}
									/>
									<label className="label label-alt label-active">
										Business District under taking
									</label>
								</div>

								<div className="input-field--wrap">
									<select
										className="input-field"
										name="projectType"
										onChange={handleFields}
									>
										<option value="" selected disabled>
											Select
										</option>
										<option value="1">
											Electricity Pole Project (low voltage)
										</option>
										<option value="2">Distribution Transformer Project</option>
										<option value="3">
											Construction of 0.415kV Line Project
										</option>
										<option value="4">Construction of 11kV Line Project</option>
										<option value="5">Construction of 33kV Line Project</option>
										<option value="6">
											Construction of 132/11kV Line Project (for TCN customers
											only)
										</option>
										<option value="7">Rural Electrification Project</option>
										<option value="8">Other Projects</option>
									</select>
									<label className="label label-alt label-active">
										Project Type
									</label>
								</div>

								<div className="input-field--wrap">
									<select
										className="input-field"
										name="estimatedValue"
										onChange={handleFields}
									>
										<option value="" selected disabled>
											Select
										</option>
										<option value="Below N1million">Below N1million</option>
										<option value="N1million – N5 million">
											N1million – N5 million
										</option>
										<option value="N5 million – N10million">
											N5 million – N10million
										</option>
										<option value="N10million – N50 million">
											N10million – N50 million
										</option>
										<option value="N50 million – N100 million">
											N50 million – N100 million
										</option>
										<option value="N100 million – N500 million">
											N100 million – N500 million
										</option>
										<option value="Above N1 billion">Above N1 billion</option>
										<option value="Not Sure">Not Sure</option>
									</select>
									<label className="label label-alt label-active">
										Estimated Value of Project
									</label>
								</div>

								<div className="input-field--wrap wrap-full">
									<textarea
										className="input-field"
										name="projectDescription"
										type="text"
										value={field.projectDescription}
										onChange={handleFields}
									></textarea>
									<label className="label label-alt label-active">
										Project Description
									</label>
								</div>

								<div className="form-others">
									<div className="documentation-wrap ">
										<h6 className="text-mini--subheading font-alt documentation-wrap--heading">
											Project Documentation
										</h6>
										<div className="optional-doc--wrap">
											<Radio
												title="Feasibility Study"
												id="feasibilityStudy"
												name="feasibilityStudy"
												value={[{ name: "Yes" }, { name: "No" }]}
												onChange={handleRadio}
											/>
											{field.feasibilityStudy.toLowerCase() !== "no" && (
												<FileUpload
													title="Upload Feasibility Study Document"
													name="feasibilityStudy"
													onChange={handleImageChange}
												/>
											)}
										</div>
										<div className="optional-doc--wrap">
											<Radio
												title="Project Design"
												id="projectDesign"
												name="projectDesign"
												value={[{ name: "Yes" }, { name: "No" }]}
												onChange={handleRadio}
											/>
											{field.projectDesign.toLowerCase() !== "no" && (
												<FileUpload
													title="Upload Project Design Document"
													name="projectDesign"
													onChange={handleImageChange}
												/>
											)}
										</div>
										<div className="optional-doc--wrap">
											<Radio
												title="Financial Projection"
												id="financialProjection"
												name="financialProjection"
												value={[{ name: "Yes" }, { name: "No" }]}
												onChange={handleRadio}
											/>
											{field.financialProjection.toLowerCase() !== "no" && (
												<FileUpload
													title="Upload Financial Projection Document"
													name="financialProjection"
													onChange={handleImageChange}
												/>
											)}
										</div>
									</div>

									<div className="documentation-wrap">
										<h6 className="text-mini--subheading font-alt documentation-wrap--heading">
											Documentations
										</h6>

										<div className="document-upload">
											<div className="single-upload">
												<div className="optional-doc--wrap">
													<h6 className="text-mini--subheading font-alt ">
														ID Card
													</h6>
													<p className="text-normal text-mini--subheading mb-4">
														You are required to provide your ID Card.
													</p>
													<FileUpload
														title="Upload ID Card"
														name="idCard"
														onChange={handleImageChange}
													/>
												</div>
											</div>
											<div className="single-upload">
												<div className="optional-doc--wrap">
													<h6 className="text-mini--subheading font-alt ">
														Utility Bill (Optional)
													</h6>
													<FileUpload
														title="Upload Utility Bill"
														name="utilityBill"
														onChange={handleImageChange}
													/>
												</div>
											</div>
										</div>
									</div>
								</div>
								<button
									className={`neip-cta ${
										checkEmpty(state) === true && "disabled"
									}`}
									type="submit"
									onClick={submitForm}
								>
									Submit
								</button>
							</div>
						</div>
					</div>
				</div>
			)}
		</div>
	);
};

const mapStateToProps = state => ({
	docs: state.form.docs
})

export default connect(mapStateToProps, null)(IndividualForm);
