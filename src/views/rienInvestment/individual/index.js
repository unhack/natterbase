import React from "react";
import PageHeader from "../../../components/general-components/page-header";
import IndividualForm from "./individualForm";




export default function IndividualInvestment() {

    const individualHeading = {
        title: "Individual Investment",
        subtitle: "We are a creative agency that focuses on brand strategy and digital design that bridges the gap between brands and the users they serve.  With our philosophy that great design establishes credibility,"
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={individualHeading} />
            <IndividualForm />
        </div>
    );
}
