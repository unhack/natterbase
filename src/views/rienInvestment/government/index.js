import React from "react";
import PageHeader from "../../../components/general-components/page-header";
import GovernmentForm from "./form";




export default function GovernmentInvestment() {

    const individualHeading = {
        title: "Government Agency",
        subtitle: ""
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={individualHeading} />
            <GovernmentForm />
        </div>
    );
}
