import React from "react";
import PageHeader from "../../general-components/page-header";
import GeneralForm from "../sections/generallForm";




export default function GeneralInvestment() {

    const heading = {
        title: "Company Organization ",
        subtitle: "We are a creative agency that focuses on brand strategy and digital design that bridges the gap between brands and the users they serve.  With our philosophy that great design establishes credibility, "
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={heading} />
            <GeneralForm />
        </div>
    );
}
