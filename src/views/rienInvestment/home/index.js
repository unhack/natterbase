import React from "react";
import HeaderFold from "../sections/headerFold";
import AboutRien from "../sections/aboutRien";
import WhyInvest from "../sections/whyInvest";



export default function RienLandingHome() {

    return (
        <div className="neip-entry--wrap">
            <HeaderFold />
            <AboutRien />
            <WhyInvest />
        </div>
    );
}
