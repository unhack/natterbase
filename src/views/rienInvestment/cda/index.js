import React from "react";
import PageHeader from "../../../components/general-components/page-header";
import CDAForm from "./form";




export default function CDAInvestment() {

    const individualHeading = {
        title: "Community Development Association",
        subtitle: ""
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={individualHeading} />
            <CDAForm />
        </div>
    );
}
