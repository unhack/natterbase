import React from "react";
import PageHeader from "../../../components/general-components/page-header";
import CorporateForm from "./form";




export default function CorporateInvestment() {

    const individualHeading = {
        title: "Corporate Organization",
        subtitle: ""
    }

    return (
        <div className="neip-entry--wrap">
            <PageHeader header={individualHeading} />
            <CorporateForm />
        </div>
    );
}
