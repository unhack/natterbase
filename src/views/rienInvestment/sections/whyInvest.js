import React from "react";
import Green from "../../../assets/img/green.svg";

export default function WhyInvest() {
    return (

        <div className="neip-standard--section neip-section--100 overflow-visible">
            <div className="container">
                <div className="neip-section flex-start fadeTextUp">
                    <div className="neip-section--40">
                        <div className="neip-small--wrap has-heading--border">
                            <h3 className="text-mini--heading  font-alt paragraph-spacer">
                                Why should i Invest in RIEN
                            </h3>


                        </div>
                    </div>
                    <div className="neip-section--60">
                        <div className="neip-steps--cta">
                            <div className="neip-step">
                                <div className="neip-step--icon">
                                    <span className="step-icon">
                                        <img src={Green} alt={"Wealth neip Steps"} />
                                    </span>
                                </div>
                                <div className="neip-step--text">
                                    <h5 className="text-mini--subheading">Life Style Savings</h5>
                                    <p className="text-opaque">You can easily integrate Monnify on your web channel and mobile app to collect payments from your customers via cards, account transfers and QR code. Our well documented API In cases where it’s needed, we dedicate a technical resource person to help you get up and running in no time.</p>
                                </div>
                            </div>
                        </div>
                        <div className="neip-steps--cta">
                            <div className="neip-step">
                                <div className="neip-step--icon">
                                    <span className="step-icon">
                                        <img src={Green} alt={"Wealth neip Steps"} />
                                    </span>
                                </div>
                                <div className="neip-step--text">
                                    <h5 className="text-mini--subheading">Children Education Savings</h5>
                                    <p className="text-opaque">Monnify delivers a unique experience for you and your customers by allowing you to collect payments offline via account transfers, cards and QR Code on a POS Machine. The Monnify channel on the POS machine displays an account number your customers can initiate an interbank transfer to and the machine prints a receipt instantly on payment completion. </p></div>
                            </div>

                        </div>
                        <div className="neip-steps--cta">
                            <div className="neip-step">
                                <div className="neip-step--icon">
                                    <span className="step-icon">
                                        <img src={Green} alt={"Wealth neip Steps"} />
                                    </span>
                                </div>
                                <div className="neip-step--text">
                                    <h5 className="text-mini--subheading">House Ownship Savings</h5>
                                    <p className="text-opaque">You can also receive payments via account transfers and cards on the Monnify Till Mobile App. This app displays an account number your customers can make an interbank transfer to and notifies you instantly in-app on payment completion.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
