import React, { useState } from "react";

const FormSample = () => {

    const [state, setState] = useState({
        file: '',
        imagePreviewUrl: '',
    })

    const handleImageChange = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            setState(prevState => ({
                ...prevState,
                file: file,
                imagePreviewUrl: reader.result
            }))
        }
        if (file) {
            reader.readAsDataURL(file);
        }

    }

    let $imagePreview = null;
    let $imageText;
    if (state.file) {
        $imagePreview = (<img src={state.imagePreviewUrl} alt="" />);
        $imageText = (<h3 className="color-secondary personalize-text text-center">+ Change Photo</h3>)
    } else {
        $imagePreview = (<div className="buddy-image--drop">

        </div>);
        $imageText = (<h3 className="color-secondary change-text personalize-text text-center">
            Personalise your goal by <br /> <span>+ Adding a photo.</span>
        </h3>)
    }

    return (
        <div className="neip-standard--section form-section neip-section--100 overflow-visible">
            <div className="container">
                <div className="neip-section">
                    <div className="neip-section--100 form-wrap">
                        <div className="form-wrap">
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Full Name</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">House Address</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Phone Number</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Email Address</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option >Company</option>
                                </select>
                                <label className="label label-alt label-active">Distribution Company</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Utitlity account number</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Meter Number</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Business District under taking</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option >Company</option>
                                </select>
                                <label className="label label-alt label-active">Project Type</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">

                                    <option >Company</option>
                                </select>
                                <label className="label label-alt label-active">Estimated Value of Project</label>
                            </div>
                            <div className="input-field--wrap wrap-full">
                                <textarea className="input-field"></textarea>
                                <label className="label label-alt label-active">Project Description</label>
                            </div>
                            <div className="form-others">
                                <div className="documentation-wrap ">
                                    <h6 className="text-mini--subheading font-alt documentation-wrap--heading">Project Documentation</h6>
                                    <div className="has-checkboxes flex flex justify-between align-center">
                                        <div>
                                            <p>Feasibility Study</p>
                                        </div>
                                        <div className="flex">
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-1" name="someRadio" type="radio" checked />
                                                <label className="custom-radio-elem" for="someRadio-1"></label>
                                                <label className="custom-radio-label" for="someRadio-1">Yes</label>
                                            </div>
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-2" name="someRadio" type="radio" />
                                                <label className="custom-radio-elem" for="someRadio-2"></label>
                                                <label className="custom-radio-label" for="someRadio-2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="has-checkboxes flex flex justify-between align-center">
                                        <div>
                                            <p>Project Design</p>
                                        </div>
                                        <div className="flex">
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-1" name="someRadio" type="radio" checked />
                                                <label className="custom-radio-elem" for="someRadio-1"></label>
                                                <label className="custom-radio-label" for="someRadio-1">Yes</label>
                                            </div>
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-2" name="someRadio" type="radio" />
                                                <label className="custom-radio-elem" for="someRadio-2"></label>
                                                <label className="custom-radio-label" for="someRadio-2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="has-checkboxes flex flex justify-between align-center">
                                        <div>
                                            <p>Financial Projection</p>
                                        </div>
                                        <div className="flex">
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-1" name="someRadio" type="radio" checked />
                                                <label className="custom-radio-elem" for="someRadio-1"></label>
                                                <label className="custom-radio-label" for="someRadio-1">Yes</label>
                                            </div>
                                            <div className="custom-radio">
                                                <input className="custom-radio-input" id="someRadio-2" name="someRadio" type="radio" />
                                                <label className="custom-radio-elem" for="someRadio-2"></label>
                                                <label className="custom-radio-label" for="someRadio-2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="documentation-wrap">
                                    <h6 className="text-mini--subheading font-alt documentation-wrap--heading">Documentations</h6>
                                    <div className="document-upload">
                                        <div className="personalize--card">
                                            <div className="previewComponent">
                                                <input className="fileInput"
                                                    type="file"
                                                    onChange={(e) => handleImageChange(e)} accept="image/*" />
                                                <div className={`${state.imagePreviewUrl === "" && "drop"} imgPreview`} >
                                                    {$imagePreview}
                                                </div>
                                            </div>
                                            {$imageText}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" className="neip-cta">
                                Submit
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default FormSample;
