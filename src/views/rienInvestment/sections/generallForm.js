import React from "react";

const GeneralForm = () => {
    return (

        <div className="neip-standard--section form-section neip-section--100 overflow-visible">
            <div className="container">
                <div className="neip-section">
                    <div className="neip-section--100">
                        <div className="form-wrap">
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Name of company</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Incorporation/Registration Details </label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Address of company</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option disabled defaultValue="" selected>Nature of business</option>
                                    <option >Company</option>
                                </select>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Name of rep</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Email address</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Phone number</label>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Position in company</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option disabled defaultValue="" selected>Distribution company</option>
                                    <option >Company</option>
                                </select>
                            </div>
                            <div className="input-field--wrap">
                                <input className="input-field" />
                                <label className="label label-alt label-active">Business District under taking</label>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option disabled defaultValue="" selected>Select project type</option>
                                    <option >Option</option>
                                </select>
                            </div>
                            <div className="input-field--wrap">
                                <select className="input-field">
                                    <option disabled defaultValue="" selected>Estimated Value of Project </option>
                                    <option >Option</option>
                                </select>
                            </div>

                            <button type="submit" className="neip-cta">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default GeneralForm;
