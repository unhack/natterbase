import React, { useState } from "react";
import Radio from "../../general-components/form/radio";
import { ReactComponent as Upload } from "../../../assets/img/upload.svg"
import Loader from "../../general-components/Loader/Loader";
import { postCall } from "../../../network";
import { urls } from "../../../network/url";
import SuccessMessage from "../../general-components/SuccessMessage/SuccessMessage";
import { Link } from "react-router-dom";
import { ReactComponent as CircleCheck } from "../../../assets/img/circleChecked.svg";

const IndividualForm = () => {
    const [field, setFields] = useState({
        feasibilityStudy: "No",
        projectDesign: "No",
        financialProjection: "No",
    })
    const [file, setFile] = useState({
        file: '',
        imagePreviewUrl: '',
    })
    const [state, setState] = useState({
        name: "",
        phone: "",
        address: "",
        email: "",

    })
    const [loading, setLoading] = useState(false);
    const [successful, setSuccessful] = useState(false)


    const handleImageChange = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            setFile(prevState => ({
                ...prevState,
                file: file,
                imagePreviewUrl: reader.result
            }))
        }
        if (file) {
            reader.readAsDataURL(file);
        }

    }

    let $imagePreview = null;
    let $imageText;
    if (file.file) {
        $imagePreview = (<div className="image-uploaded"><CircleCheck className="checked-uploaded" /><img src={file.imagePreviewUrl} alt="" /></div>);
        $imageText = (<div className="full-width upload-text">
            <h3 className="color-secondary change-text personalize-text text-center">
                <span><Upload /> Change Document</span>
            </h3>
        </div>)
    } else {
        $imagePreview = (<div className="buddy-image--drop">

        </div>);
        $imageText = (<div className="full-width upload-text">
            <h3 className="color-secondary change-text personalize-text text-center">
                <span><Upload /> Upload Document</span>
            </h3>
            <p className="text-opaque text-center text-sm">Supports .png, jpg</p>
        </div>)
    }


    const handleRadio = (event) => {
        setFields({
            ...field,
            [event.target.name]: event.target.value   //define new key-value pair with new uuid and [].
        })

    }

    const handleFields = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value
        })
    }

    const submitForm = async (e) => {
        setLoading(true);
        const data = {
            "email": state.email,
            "password": state.password,
            "last_name": state.lastName,
            "first_name": state.firstName,
            "type": "investor",
            "mobile": state.phone
        }

        const finalData = JSON.stringify(data);
        const response = await postCall(urls.rienRegistration, finalData);
        if (typeof response !== "undefined" && response.data.success === true) {
            setLoading(false);
            setSuccessful(true);
        } else {
            setLoading(false);
            setSuccessful(true);
            // toaster.notify("This email is already registered", {
            //     duration: null
            // });
        }


    }


    if (loading) {
        return (
            <Loader>
                <h2>Registration in Progress</h2>
                <p>Hold on to your seatbelt, its about <br /> to be a journey </p>
            </Loader>
        );
    }
    return (

        <div className="neip-standard--section form-section neip-section--100 overflow-visible">
            {successful ? (
                <SuccessMessage>
                    <h2>Registration Successfully</h2>
                    <p>
                        Your application for an Individual Rien Investment has been received.
                        We'll be in touch once your application has been reviewed. </p>

                    <Link to="/rien-investment" className="neip-cta neip-cta--lg neip-cta--center">Okay</Link>

                </SuccessMessage>

            ) :
                <div className="container">
                    <div className="neip-section">
                        <div className="neip-section--100 form-wrap">

                            <div className="form-wrap">
                                <div className="input-field--wrap">
                                    <input className="input-field" name="name" type="text" value={field.name} onChange={handleFields} />
                                    <label className="label label-alt label-active">Full Name</label>
                                </div>
                                <div className="input-field--wrap">
                                    <input className="input-field" name="address" type="text" value={field.address} onChange={handleFields} />
                                    <label className="label label-alt label-active">House Address</label>
                                </div>
                                <div className="input-field--wrap">
                                    <input className="input-field" name="phone" type="tel" value={field.phone} onChange={handleFields} />
                                    <label className="label label-alt label-active">Phone Number</label>
                                </div>
                                <div className="input-field--wrap">
                                    <input className="input-field" name="email" type="email" value={field.email} onChange={handleFields} />
                                    <label className="label label-alt label-active">Email Address</label>
                                </div>

                                <div className="input-field--wrap">
                                    <select className="input-field" name="distributionCompany" onChange={handleFields}>
                                        <option value="" selected disabled>Select</option>
                                        <option value="Company" >Company</option>
                                        <option value="Company" >Company</option>
                                    </select>
                                    <label className="label label-alt label-active">Distribution Company</label>
                                </div>

                                <div className="input-field--wrap">
                                    <input className="input-field" name="utilityAccountNumber" type="tel" value={field.utilityAccountNumber} onChange={handleFields} />
                                    <label className="label label-alt label-active">Utility account number</label>
                                </div>

                                <div className="input-field--wrap">
                                    <input className="input-field" name="meterNo" type="tel" value={field.meterNo} onChange={handleFields} />
                                    <label className="label label-alt label-active">Meter Number</label>
                                </div>

                                <div className="input-field--wrap">
                                    <input className="input-field" name="businessDistrict" type="text" value={field.businessDistrict} onChange={handleFields} />
                                    <label className="label label-alt label-active">Business District under taking</label>
                                </div>

                                <div className="input-field--wrap">
                                    <select className="input-field" name="projectType" onChange={handleFields}>
                                        <option value="" selected disabled>Select</option>
                                        <option value="company">Company</option>
                                    </select>
                                    <label className="label label-alt label-active">Project Type</label>
                                </div>

                                <div className="input-field--wrap">
                                    <select className="input-field" name="estimatedValue" onChange={handleFields}>
                                        <option value="" selected disabled>Select</option>
                                        <option >Company</option>
                                    </select>
                                    <label className="label label-alt label-active">Estimated Value of Project</label>
                                </div>

                                <div className="input-field--wrap wrap-full">
                                    <textarea className="input-field" name="projectDescription" type="text" value={field.projectDescription} onChange={handleFields}></textarea>
                                    <label className="label label-alt label-active">Project Description</label>
                                </div>
                                <div className="form-others">
                                    <div className="documentation-wrap ">
                                        <h6 className="text-mini--subheading font-alt documentation-wrap--heading">Project Documentation</h6>
                                        <Radio title="Feasibility Study" id="feasibilityStudy" name="feasibilityStudy" value={[{ name: "Yes" }, { name: "No" }]} onChange={handleRadio} />
                                        <Radio title="Project Design" id="projectDesign" name="projectDesign" value={[{ name: "Yes" }, { name: "No" }]} onChange={handleRadio} />
                                        <Radio title="Financial Projection" id="financialProjection" name="financialProjection" value={[{ name: "Yes" }, { name: "No" }]} onChange={handleRadio} />
                                    </div>
                                    <div className="documentation-wrap">
                                        <h6 className="text-mini--subheading font-alt documentation-wrap--heading">Documentations</h6>

                                        <div className="document-upload">
                                            <div className="single-upload">
                                                <h6 className="text-mini--subheading font-alt ">Utility Bill</h6>
                                                <p className="text-normal text-mini--subheading mb-4">You are required to provide your utility bill.</p>
                                                <div className="personalize--card">
                                                    <div className="previewComponent">
                                                        {$imageText}
                                                        <input className="fileInput"
                                                            type="file"
                                                            onChange={(e) => handleImageChange(e)} accept="image/*" />
                                                        <div className={`${file.imagePreviewUrl === "" && "drop"} imgPreview`} >

                                                        </div>
                                                    </div>
                                                    {$imagePreview}
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <button type="submit" className="neip-cta" onClick={submitForm}>
                                    Submit
                            </button>
                            </div>


                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default IndividualForm;
