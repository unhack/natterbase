import React from "react";
import { ReactComponent as Art } from "../../../assets/img/entry-investment.svg";
import RienImage from "../../../assets/img/entry-investment.png";

export default function HeaderFold() {
	return (
		<div className="neip-entry--fold neip-section--100">
			<div className="container">
				<div className="neip-section fadeTextUp">
					<div className="neip-section--50">
						<div className="entry-text--wrap">
							<p className="text-subheading text-semi--bold">What is</p>
							<h1 className="text-heading color-primary capitalize">
								RIEN Investment
							</h1>
							<p className="text-opaque">
								We are a creative agency that focuses on brand strategy and
								digital design that bridges the gap between brands and the users
								they serve. With our philosophy that great design establishes
								credibility,
							</p>
						</div>
					</div>
					<div className="neip-section--50">
						<div className="entry-illustration ">
							<img className="" src={RienImage} alt="Wealth neip landing" />
							<Art className="art" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
