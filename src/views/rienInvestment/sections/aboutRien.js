import React from "react";

export default function AboutRien() {
    return (

        <div className="neip-standard--section neip-section--100 standard--bg overflow-visible">
            <div className="container">
                <div className="neip-section fadeTextUp">
                    <div className="neip-section--40">
                        <div className="neip-small--wrap has-heading--border color-white">
                            <h3 className="text-mini--heading  font-alt paragraph-spacer">
                                What is RIEN?
                            </h3>
                            <p className="text-subheading text-opaque">Investment plan for future investors </p>

                        </div>
                    </div>
                    <div className="neip-section--60">
                        <div className="neip-large--wrap color-white">
                            <p className="text-normal text-mini--subheading">NEIP is a payment gateway for businesses to accept payments from customers, either on a recurring basis or one-time. Monnify offers an easier, faster and cheaper way for businesses to get paid on their web channel and mobile applications using convenient payment methods for customers with the highest success rates obtainable in Nigeria.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
