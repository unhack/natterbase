import React from 'react';
import HeaderStyle from './Header.styles';
//import {  useHistory } from 'react-router-dom';

const Header = (props) => {
	//const history = useHistory();
	return (
		<HeaderStyle>
			<div className='top-nav--item align-items-center'>
				<div className='navigation'>
					<div className='back'>
						<span className='icon'>
							<i className='fas fa-arrow-left'></i>
						</span>
						<span className='text'>Back</span>
					</div>
					{/* <div className="already_have_account">
                        <Link className="d-flex align-items-center" to="/login">
                            <span className="text">
                                Already have an account? &nbsp;&nbsp;Login{" "}
                            </span>
                            <span className="icon">
                                <RightArrow />
                            </span>
                        </Link>
                    </div> */}
				</div>
			</div>
		</HeaderStyle>
	);
};

export default Header;
