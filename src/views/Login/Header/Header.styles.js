import Styled from "styled-components";

const HeaderStyle = Styled.nav`
    .container {
        display: flex;
        justify-content: space-between;
        align-items: center;
        max-width: 1250px;
    }
    .navigation {
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 23px;
        width: 100%;
        max-width: 700px;
    }

    @media only screen and (max-width: 1340px) {
        .navigation {
            max-width: 625px;
        } 
    }
    .navigation .back, .navigation .already_have_account {
        display: flex;
        align-items: center;
    }

    .navigation .back .text, .navigation .already_have_account .text {
        font-weight: 600;
        font-size: 14px;
        line-height: 28px;
        letter-spacing: 0.05em;
        color: ${({ theme }) => theme.bodyTextColor}
    }

        @media only screen and (max-width: 1199px) {
            .navigation {
                    max-width: 570px;   
            } 
        }


    .navigation .back .icon {
        margin-right: 12px;
    }
    .navigation .already_have_account .icon {
        margin-left: 10px;
    }
`;

export default HeaderStyle;
