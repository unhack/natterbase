import React from 'react';
import WhiteLogo from '../../../assets/img/logo-white.png';
import { ReactComponent as Quote } from '../../../assets/img/quote.svg';

const OnboardingSidebar = () => {
	return (
		<div className='onboarding--sidebar'>
			<div className='neip-sidebar--wrap'>
				<div className='top-nav--item'>
					<img src={WhiteLogo} alt='Neip Investments Logo' />
				</div>
				<div className='excerpt-text neip-small--wrap color-white'>
					<Quote className='quote' />
					<p className='text-subheading text-medium'>
						A big part of financial freedom is having your heart and mind free
						from worry about the what-ifs of life.
					</p>
					<span className='text-sm'>- Maria Amadu Jenner</span>
				</div>
				<div className='bottom-onboarding--spacer'></div>
			</div>
		</div>
	);
};
export default OnboardingSidebar;
