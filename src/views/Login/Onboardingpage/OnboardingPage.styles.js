import Styled from "styled-components";

const OnboardingPageStyle = Styled.div`
    background-color: #E4EAE7;
    min-height: 100vh;
    width: 100%;
    display: flex;
    position: relative;
   
    @media only screen and (max-width: 1024px) { 
        flex-direction: column;
        padding: 20px
    }

    @media only screen and (min-width: 1025px) and (max-height: 771px) {
        min-height: 830px;
    }
`;

export default OnboardingPageStyle;
