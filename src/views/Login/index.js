import React from 'react';
import Card from '../../components/general-components/Card/Card';
import Input from '../../components/general-components/Input/Input';
import Button from '../../components/general-components/Button/Button';
import LoginStyle from './Login.styles';
import { Link } from 'react-router-dom';
import 'toasted-notes/src/styles.css'; // optional styles
import OnboardingPageStyle from './Onboardingpage/OnboardingPage.styles';
import OnboardingSidebar from './Onboardingpage/sidebar';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import './index.css';

const Login = (props) => {
	return (
		<OnboardingPageStyle>
			<OnboardingSidebar />
			<div className='onboarding-content--wrap'>
				<Header {...props} />
				<Card className='fadeIn'>
					<LoginStyle>
						<div className='top_text'>
							<h1>Login to your account</h1>
							<p>Enter your credentials to access your account.</p>
						</div>
						<form className='form-wrap'>
							<Input label='Email Address' name='email' type='email' />
							<Input label='Password' type='password' name='password' />
							<Button text='Login' color='green' fullWidth='fullWidth' />
						</form>
						<p className='forgot_password'>
							<Link to='signup/forgot-password'>Forgot Password</Link>
						</p>
					</LoginStyle>
				</Card>
			</div>
			<Footer />
		</OnboardingPageStyle>
	);
};
export default Login;
