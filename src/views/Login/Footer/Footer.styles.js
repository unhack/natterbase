import Styled from "styled-components";

const FooterStyle = Styled.nav`
    padding: 0;
    position: fixed;
    width: 100%;
    bottom: 0;
    z-index: 9;
    @media only screen and (max-width: 1024px) {
        position: static;
        margin-top: 50px
    }

    @media only screen and (min-width: 1025px) and (max-height: 771px) { 
        position: absolute;
    }
    .container {
        max-width: 1250px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    

    .left_menu {
        display: flex;
        align-items: center;
        justify-content: space-between;
        min-width: 566px;
        width: 38%;
        padding-left: 75px;
    }

    .left_menu div {
        display: flex;
        align-items: center;
    }

    .left_menu .footer_logo a {
        margin-left: 15px;
            font-size: 14px;
            line-height: 16px;
        }

    .left_menu .footer_links a {
    font-size: 14px;
    line-height: 12px;
    }

    .footer_links a {
        margin-right: 45px;
    }

    .right_menu {
        align-items: center;
    }

    .right_menu a {
        opacity: 0.6;
        display: flex
    }

    .right_menu a:not(:last-child) {
        margin-right: 36px;
    }

    .right_menu svg {
        width: 21px;
        height: 21px;
    }

    @media only screen and (max-width: 1340px) {
        .right_menu {
            max-width: 550px;
        } 
    }

`;

export default FooterStyle;
