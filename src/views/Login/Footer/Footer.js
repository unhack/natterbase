import React from 'react';
import FooterStyle from './Footer.styles';

const Footer = (props) => {
	return (
		<FooterStyle>
			<div className='onboarding-footer'>
				<div className='left_menu color-white'>
					<div className='footer_logo'>
						{/* <FooterIcon /> */}
						FooterIcon
						<a href='#!'>Copyright | 2019</a>
					</div>
					<div className='footer_links'>
						<a href='#!'>Terms</a>
						<a href='#!'>Privacy</a>
						<a href='#!'>Help</a>
					</div>
				</div>
				<div className='right_menu'>
					<a href='#!'>
						{' '}
						<i className='fas fa-facebook'></i>
					</a>
					<a href='#!'>
						<i className='fab fa-twitter'></i>
					</a>
					<a href='#!'>
						<i className='fas fa-instagram'></i>
					</a>
				</div>
			</div>
		</FooterStyle>
	);
};

export default Footer;
