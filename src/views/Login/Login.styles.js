import Styled from "styled-components";

const LoginStyle = Styled.div`
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
    .top_text {
        margin-bottom: 57px;
    }
    .top_text h1 {
        font-size: 28px;
        line-height: 160%;
        color: #03293D;
        font-weight: 500;
        text-align: center;
    }
    .top_text p {
        font-weight: 500;
        font-size: 14px;
        color: #03293D;
        opacity: 0.4;
        line-height: 180%;
        text-align: center;
    }
    .names {
        display: flex;
        justify-content: space-between;
    }
    h3 {
        font-weight: 500;
        font-size: 18px;
        color: #03293D;
        line-height: 160%;
        margin-bottom: 10px;
    }
    .date_of_birth {
        display: flex;
    }
    .date_of_birth input:not(last-child){
        margin-right: 30px;
    }
    .forgot_password a {
        display: block;
        text-align: center;
        font-weight: 500;
        font-size: 14px;
        line-height: 160%;
        padding: 34px 0;
        color: #D12631;
        text-decoration: none;
    }
`;

export default LoginStyle;
