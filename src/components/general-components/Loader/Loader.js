import React from "react";
import LoaderStyle from "./Loader.styles";

const Loader = (props) => {
    return (
        <LoaderStyle>
            <div className="spinner-wrap">
                <span className="spinner"></span>
            </div>
            {props.children}
        </LoaderStyle>
    );
};

export default Loader;
