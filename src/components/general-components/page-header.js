import React from "react";
import { ReactComponent as HeaderShapes } from "../../assets/img/header-shapes.svg"

const PageHeader = (props) => {
    return (

        <div className="neip-standard--section page-header--section neip-section--100 growth-mock--section">
            <div className="container">
                <div className="neip-section">
                    <div className="neip-section--100">
                        <div className="neip-standard--wrap text-center fadeTextUp">
                            {props.header.title !== "" &&
                                <h1 className={`text-heading color-primary ${props.header.subtitle !== "" ? "heading-spacer" : ""}`}>
                                    {props.header.title}
                                </h1>
                            }
                            {props.header.subtitle !== "" &&
                                <p className="text-opaque">{props.header.subtitle}</p>
                            }
                        </div>
                    </div>

                </div>
            </div>
            <HeaderShapes className="header-shapes" />
        </div>
    );
}

export default PageHeader;
