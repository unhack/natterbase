import Styled from "styled-components";

const CardStyle = Styled.div`
	background: #fcfcfc;
	/* Card Shadow */

	box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.03);
    border-radius: 15px;
    margin-top: 28px;
    padding: 32px 25px;
    overflow: hidden;
    overflow-x: scroll;

    .cta {
        background: #2DB875;
        inline-size: 112px;
        block-size: 41px;
        border-radius: 5px
        background: #2DB875;
        border: 0;
        font-weight: 500;
        font-size: 14px;
        line-height: 160%;
        color: #FFFFFF;
        cursor: pointer;
    }

    /* TABLE */
    table {
        inline-size: 100%;
    }

    table th {
        padding: 0rem 1.5rem 1rem;
    }

    table td {
        padding: 1rem 1.5rem;
    }

    tbody tr:hover, tr:focus, tr:focus-within {
        background: #1a0c2f11;
    }

    tbody tr {
        border-radius: 5px;
    }

    td {
        font-size: 16px;
        color: #182538;
        text-align: left;
    }
    
    .table thead th {
        text-align: left;
        font-size: 15px;
        color: #182538;
        opacity: 0.5;
        font-weight: 200;
        letter-spacing: 0.2px;
        font-family: 'Futura', sans-serif;
    }

    tr td:last-child {
        text-align: right;
    }

    tr th:nth-child(4), tr td:nth-child(4) {
        text-align: center
    }

    .pagination {
        font-family: 'Futura', sans-serif;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        margin-top: 35px;
        inline-size: 100%;
        padding: 0 5px;
    }

    @media screen and (max-width: 768px) {
        .pagination {
            justify-content: flex-start;
        }
    }

    .pagination ul {
        display: inline-flex;
        margin: 0 20px; 
    }

    .pagination ul li {
        margin-right: 20px;
        width: 29px;
        height: 29px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .pagination ul li.active {
        background:  #000000;
        color: #FFFFFF;
    }

    .pagination-button {
        background: transparent;
        border: 0;
        display: flex;
        flex-direction: row;
        align-items: center
    }

    .pagination-icon--left {
        transform: rotate(90deg)
    }

    .pagination-icon--right {
        transform: rotate(-90deg)
    }
`;

export default CardStyle;
