import React from "react";
import CardStyle from "./Card.styles.js";

const Card = ({ children }) => <CardStyle>{children}</CardStyle>;

export default Card;
