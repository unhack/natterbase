import React from 'react';
import './Pagination.css';

const Pagination = () => {
	return (
		<>
			<div className='pagination__component'>
				<div className='pagination__bar'>
					<div className='prev-section'>
						<i className='fas fa-angle-left'></i>
						<span className='prev'>Previous</span>
					</div>
					<div className='number-section'>
						<span className='number active'>1</span>
						<span className='number'>2</span>
						<span className='number'>3</span>
						<span className='number'>4</span>
					</div>
					<div className='next-section'>
						<span className='next'>Next</span>
						<i className='fas fa-angle-right'></i>
					</div>
				</div>
			</div>
		</>
	);
};

export default Pagination;
