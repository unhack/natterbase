import React from "react";
import { postFormCall } from "../../network";
import { urls } from "../../network/url";
import { connect } from 'react-redux';
import { changeDocs } from '../../redux/actions/formActions';

const useFileHook = ({changeDocs, docs}) => {
    
	const [file, setFile] = React.useState({
		file: "",
		imagePreviewUrl: "",
		url: "",
		isDoc: false,
		isLoading: false,
	});
	
	const handleImageChange = (e) => {
		let reader = new FileReader();
		let file = e.target.files[0];
		const fileType = file["type"];
		const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
		if (!validImageTypes.includes(fileType)) {
			setFile((prevState) => ({
				...prevState,
				isDoc: true,
			}));
        };
        
		const stateName = e.target.name;
		reader.onloadend = async () => {
			setFile((prevState) => ({
				...prevState,
				isLoading: true,
			}));
			let headers = { "Content-Type": "multipart/form-data" };

			let bodyFormData = new FormData();
			bodyFormData.append("file", file);

			let response = await postFormCall(urls.upload, bodyFormData, headers);
			if (typeof response !== "undefined" && response.data.success === true) {
				setFile((prevState) => ({
					...prevState,
					[stateName]: file,
					imagePreviewUrl: reader.result,
					[`${stateName}Url`]: `http://178.62.34.18:3333/uploads/${response.data.data.path}`,
					isLoading: false,
				}));

				changeDocs({
					...docs,
					[`${stateName}Url`]: `http://178.62.34.18:3333/uploads/${response.data.data.path}`,
				});
			} else {
                return
			};
		};
		if (file) {
			reader.readAsDataURL(file);
		}
	};

	return { file, handleImageChange, docs };
};

const mapStateToProps = state => ({
	docs: state.form.docs
});

const mapDispatchToProps = dispatch => ({
    changeDocs: data => dispatch(changeDocs(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(useFileHook);




