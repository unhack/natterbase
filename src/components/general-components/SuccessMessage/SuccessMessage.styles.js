import Styled from "styled-components";

const SuccessMessageStyle = Styled.div`
width: 100vw;
height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #fff;
    z-index: 999;
display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: auto;
h2 {
        font-weight: 500;
font-size: 24px;
line-height: 32px;
text-align: center;
color: #03293D;
margin-top: 23px;
margin-bottom: 11px;
    }
    p {
        font-weight: normal;
font-size: 16px;
line-height: 150%;
text-align: center;
color: #03293D;
opacity: 0.5;
margin-bottom: 30px;
    }
`;

export default SuccessMessageStyle;
