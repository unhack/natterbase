import React from "react";
import SuccessMessageStyle from "./SuccessMessage.styles";
import { ReactComponent as CircleCheck } from "../../../assets/img/circleChecked.svg";

const SuccessMessage = (props) => (
    <SuccessMessageStyle>
        <CircleCheck className="circle_check" />
        <div className="success-max">
            {props.children}
        </div>
    </SuccessMessageStyle>
);

export default SuccessMessage;
