import React from 'react';

import './Button.css';

const Button = ({ text, color, fullWidth, children }) => {
	return (
		<button className={`default ${color} ${fullWidth}`}>
			{text || children}
		</button>
	);
};

export default Button;
