import React from 'react';

import './Input.css'

const TextField = ({type, placeholder, text, value, name, handleChange, className}) => {
    return (
        <input 
            type={type} 
            className={`textfield ${className}`}
            placeholder={placeholder} 
            value={value} 
            name={name}
            onChange={handleChange}>{text}</input>
    )
}

export default TextField;