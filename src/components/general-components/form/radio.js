import React from "react";


const Radio = (props) => {
    return (
        <div className="has-checkboxes flex flex justify-between align-center" onChange={event => props.onChange(event)}>
            <div>
                <p>{props.title}</p>
            </div>
            <div className="flex justify-end">
                {props.value.map((item, index) =>
                    <div className="custom-radio" key={index}>
                        <input className="custom-radio-input" id={`${props.id}${index + 1}`} name={props.name} type="radio" defaultChecked={index === 1} value={item.name} />
                        <label className="custom-radio-elem" htmlFor={`${props.id}${index + 1}`}></label>
                        <label className="custom-radio-label" htmlFor={`${props.id}${index + 1}`}>{item.name}</label>
                    </div>
                )}
            </div>
        </div>
    );
}

export default Radio;
