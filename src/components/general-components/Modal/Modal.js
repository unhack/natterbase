import React from 'react';

import './Modal.css';
import TextField from '../Input/Input';
import Button from '../Button/Button';

const Modal = () => {
    return (
        <div className='modal__component'>
            <div className='modal__content'>
                <div>
                    <p className='close__modal'><i className='fas fa-times'></i> Close </p>
                    <h4 className='modal__title'> Create New ECP</h4>
                    <div className='modal__card'>
                        <form>
                            <div>
                                <label>Company Name</label>
                                <TextField
                                    name='company'
                                    placeholder=''
                                    type='text'
                                    value=''
                                    handleChange=''
                                    className='no-border grey-bg'
                                />
                            </div>
                            <div>
                                <label>Email Address</label>
                                <TextField
                                    name='email'
                                    placeholder=''
                                    type='text'
                                    value=''
                                    handleChange=''
                                    className='no-border grey-bg'
                                />
                            </div>
                            <Button 
                                text='Submit'
                                color='green'
                                fullWidth='fullWidth'
                            />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal