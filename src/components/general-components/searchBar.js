import React from "react";
import { ReactComponent as SearchIcon } from "../../assets/icons/searchIcon.svg";
const SearchBar = ({placeholder}) => {
	return (
		<form className="row">
			<SearchIcon />
			<div>
				<input
					type="text"
					placeholder="Search"
					className={`search--input ${placeholder}`}
				></input>
			</div>
		</form>
	);
};

export default SearchBar;
