import React from "react";
import { ReactComponent as Upload } from "../../assets/img/upload.svg"
import { ReactComponent as CircleCheck } from "../../assets/img/circleChecked.svg";
import { ReactComponent as Doc } from "../../assets/img/doc.svg";
//import useFileHook from "./uploadHook";

const FileUpload = (props) => {

    //const { file, handleImageChange } = useFileHook();
    const file = ''
    const handleImageChange = () => {

    }
    
    let $imagePreview = null;
    let $imageText;
    if (file[props.name]) {
        $imagePreview = (
            <div className="image-uploaded">
                <CircleCheck className="checked-uploaded" />
                {file.isDoc ?
                    <div className="isDoc"><Doc /></div> : <img src={file.imagePreviewUrl} alt="" />
                }</div>)
            ;
        $imageText = (<div className="full-width upload-text">
            <h3 className="color-secondary change-text personalize-text text-center">
                <span><Upload /> Change Document</span>
            </h3>
        </div>)
    } else {
        $imagePreview = (<div className="buddy-image--drop">

        </div>);
        $imageText = (<div className="full-width upload-text">
            <h3 className="color-secondary change-text personalize-text text-center">
                <span><Upload /> {props.title}</span>
            </h3>
            <p className="text-opaque text-center text-sm">Supports .png, jpg</p>
        </div>)
    }

    return (
        <div className="personalize--card">
            <div className={`previewComponent ${file.isLoading ? "uploading" : ""}`}>
                {$imageText}
                <input className="fileInput" name={props.name}
                    type="file"
                    onChange={(e) => handleImageChange(e)} accept=".xlsx,.xls,image/jpg, image/png, image/jpeg,.doc, .docx,.ppt, .pptx,.txt,.pdf" />
                <div className={`${file.imagePreviewUrl === "" && "drop"} imgPreview`} >
                </div>
            </div>
            {$imagePreview}
        </div>
    );
}

export default FileUpload;
