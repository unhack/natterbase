import { combineReducers} from 'redux';
//import userReducer from './user/userReducer.js';
import { formReducer } from './reducers/formReducer'
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage'; 


export const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['form']
}



export const rootReducer =  combineReducers({
    form: formReducer
})

export default persistReducer(persistConfig, rootReducer)
