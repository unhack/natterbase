const initialState = {
	mobile: false,
	docs: {
		projectDesign: "",
		feasibilityStudy: "",
		financialProjection: "",
		utilityBill: "",
		idCard: "",
		incorporationDoc: "",
	},
	donationsData: [],
};

export const formReducer = (state = initialState, action) => {
	switch (action.type) {
		case "CHANGE_MOBILE":
			return {
				...state,
				mobile: action.newMobile,
			};
		case "CHANGE_DOCS":
			return {
				...state,
				docs: action.newPayload,
			};
		case "CHANGE_DONATIONS":
			return {
				...state,
				donationsData: action.newPayload,
			};
		default:
			return state;
	}
};