import axios from "axios";

export const postFormCall = async (url, data, headers) => {
    return axios({
        method: "post",
        url: url,
        data: data,
        headers: headers
    }).catch(error => {
        if (error.toString().includes("Network Error")) {
            // Handle Network Error
            return false
        }
        else if (error.toString().includes("401")) {
            // Handle 401 Error
            return "Token Expired";
        }
        else {
            return error.response
            // Handle General Error
        }
    });
}

export const postCall = async (url, data, headers) => {

    let finalData;
    finalData = data;
    headers = Object.assign("");

    return axios({
        method: "post",
        url: url,
        data: finalData,
        headers: headers
    }).catch(error => {
        if (error.toString().includes("Network Error")) {
            // Handle Network Error
            return false
        }
        else if (error.toString().includes("401")) {
            // Handle 401 Error
            return "Token Expired";
        }
        else {
            return error.response
            // Handle General Error
        }
    });
}

export const getCall = async (url, params, headers) => {
    return axios({
        method: "get",
        url: url,
        params: params,
        headers: headers
    }).catch((error) => {
        if (error.toString().includes("Network Error")) {
            // Handle Network Error
            return false;
        }
        else if (error.toString().includes("401")) {
            // Handle 401 Error
            return "Token Expired";
        }
        else {
            return error.response
        }

    });

}
