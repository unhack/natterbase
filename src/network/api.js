import { getCall } from ".";
import { urls } from "./url";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css"; // optional styles

export const getDonations = async () => {
	const response = await getCall(urls.getDonations);
	if (typeof response !== "undefined" && response.data.success === true) {
		return response.data.data.data;
	} else {
		toaster.notify("Please provide all required fields", {
			duration: null,
			position: "bottom",
		});
		return false;
	}
};
