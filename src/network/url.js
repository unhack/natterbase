export const urls = {
    login: "/auth/login",
    signup: "/auth/register",
    addBvn: "/profile/bvn/verify",
    rienRegistration: "/rien/create",
    upload: "/upload",
    getDonations: "/donation/projects"
}